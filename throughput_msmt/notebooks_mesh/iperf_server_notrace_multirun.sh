#!/bin/bash

DSTROOT="${PWD}"
CAPTUREFOLDER="/tmp"
IPERFLOGFILE="${CAPTUREFOLDER}/iperf.log"
IPERFCLIENTIP="192.168.123.101"
IPERFSERVERIP="192.168.123.102"

HT=$1	# entweder: 20 / 40
MCS=$2	# MCS Index: 1 ... 7 ... 15 ...
TCP=$3	# entweder: "tcp" oder "1M", "2M", "14.5M"

RUN=$4	# Start-ID des 1. Runs
RUNS=$5	# Anzahl Runs

for i in $(seq $RUN 1 $(($RUN + $RUNS - 1))); do

	# iperf 3 server starten
	# -s Server
	# -1 nach einer Verbindung beenden
	# -J Ausgabe im JSON Format
	# -V more verbose
	# -I PID File erzeugen (+ Pfad)
	# -i Ausgabe Intervall
	# --logfile logfile erzeugen (+ Pfad)
	#
	#timeout $IPERFTIMEOUT iperf3 -s -1 -J -V -I $IPERFPIDFILE -i 0.1 --logfile $IPERFLOGFILE
	iperf3 -s $IPERFSERVERIP -1 -V -i 1 --logfile $IPERFLOGFILE

	# Angefallene Dateien verschieben
	if [[ "x${TCP}" == "xtcp" ]]; then
		destinationfolder="${DSTROOT}/HT${HT}_MCS${MCS}_TCP"
	else
		destinationfolder="${DSTROOT}/HT${HT}_MCS${MCS}_UDP-${TCP}"
	fi

	FILELIST=$(ls -d $CAPTUREFOLDER/*.log)
	for file_ in $FILELIST; do
		mkdir -p ${destinationfolder}/Run${i}/
		mv ${file_} ${destinationfolder}/Run${i}/SERVER_$(basename $file_)
	done

done

