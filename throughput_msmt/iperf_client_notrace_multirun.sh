#!/bin/bash

DSTROOT="${PWD}/Messungen"
CAPTUREFOLDER="/tmp"

IPERFCLIENTIP="192.168.123.40"
IPERFSERVERIP="192.168.123.48"
IPERFTIMEOUT=100s
IPERFDURATION=60 # transmission duration in seconds

HT=$1	# entweder: 20 / 40
MCS=$2	# MCS Index: 1 ... 7 ... 15 ...
TCP=$3	# entweder: "tcp" oder "1M", "2M", "14.5M"

RUN=$4	# Start-ID des 1. Runs
RUNS=$5	# Anzahl Runs

sta_mpath_logging () {
	run=$1
	time_val=1
	echo -e "\n" > ${CAPTUREFOLDER}/Run${run}/mpath.log
	echo -e "\n" > ${CAPTUREFOLDER}/Run${run}/station.log
	while true
	do
		echo -e "\n$time_val ----------------------------------------\n" >> ${CAPTUREFOLDER}/Run${run}/mpath.log
		echo -e "\n$time_val ----------------------------------------\n" >> ${CAPTUREFOLDER}/Run${run}/station.log
		iw dev wlan0 mpath dump >> ${CAPTUREFOLDER}/Run${run}/mpath.log
		iw dev wlan0 station dump >> ${CAPTUREFOLDER}/Run${run}/station.log
		time_val=$(($time_val+1))
		if [[ $time_val -gt $IPERFDURATION ]]; then
			break
		fi
	sleep 1
	done
}

sleep 5s

for i in $(seq $RUN 1 $(($RUN + $RUNS - 1))); do

	echo 3 > /proc/sys/vm/drop_caches

	mkdir -p ${CAPTUREFOLDER}/Run${i}
	IPERFLOGFILE="${CAPTUREFOLDER}/Run${i}/iperf.log"

	# STA/MPATH logging
	sta_mpath_logging $i &
	pid=$!

	# iperf 3 client starten
	# -n Datenmenge	# -t Zeit # -k Anzahl Packets (Blocks)
	# -c server IP
	# --cport verwendeter TCP port 
	# --bind an das entsprechende interface binden (ueber IP Adresse)
	# -i log interval
	# -J Ausgabe im JSON Format
	# -V more verbose
	# --logfile Pfad zum log
	# -w TCP window or UDP socket buffer
	# -l length of r/w buffer (default: 128kB TCP / 1448 bytes UDP; max 65507 bytes UDP)
	# -Z use zerocopy API
	# -O omit certain number of initial seconds for calculating results
	# -N set nodelay option (disable Nagle)
	# -S set ToS class
	# -C set TCP cong. control alg.
	#
	#timeout $IPERFTIMEOUT iperf3 -c $IPERFSERVERIP -n 30M --cport $CLIENT_IPERF_PORT --bind $(iam ip) -i 0.1 -J -V --logfile $IPERFLOGFILE
	if [[ "x${TCP}" == "xtcp" ]]; then
		timeout $IPERFTIMEOUT iperf3 -c $IPERFSERVERIP -t $IPERFDURATION -i 1 -V --logfile $IPERFLOGFILE -Z -O 2 # -w 1M # -w 2M
	else
		timeout $IPERFTIMEOUT iperf3 -c $IPERFSERVERIP -t $IPERFDURATION -u -b $TCP -i 1 -V --logfile $IPERFLOGFILE -Z -O 2 -w 1M # -w 2M # -w 4M # -l 65507
	fi

	kill -9 $pid

	# Angefallene Dateien verschieben
	if [[ "x${TCP}" == "xtcp" ]]; then
		destinationfolder="${DSTROOT}/HT${HT}_MCS${MCS}_TCP"
	else
		destinationfolder="${DSTROOT}/HT${HT}_MCS${MCS}_UDP-${TCP}"
	fi

	FILELIST=$(ls -d $CAPTUREFOLDER/Run${i}/*.log)
	for file_ in $FILELIST; do
		mkdir -p ${destinationfolder}/Run${i}
		mv ${file_} ${destinationfolder}/Run${i}/CLIENT_$(basename $file_)
	done
	rmdir $CAPTUREFOLDER/Run${i}

	sleep 5s

done

