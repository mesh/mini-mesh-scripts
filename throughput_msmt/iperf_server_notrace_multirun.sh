#!/bin/bash

DSTROOT="${PWD}/Messungen"
CAPTUREFOLDER="/tmp"

IPERFCLIENTIP="192.168.123.40"
IPERFSERVERIP="192.168.123.48"
IPERFTIMEOUT=100s
IPERFDURATION=60 # transmission duration in seconds

HT=$1	# entweder: 20 / 40
MCS=$2	# MCS Index: 1 ... 7 ... 15 ...
TCP=$3	# entweder: "tcp" oder "1M", "2M", "14.5M"

RUN=$4	# Start-ID des 1. Runs
RUNS=$5	# Anzahl Runs

sta_mpath_logging () {
	run=$1
	time_val=1
	echo -e "\n" > ${CAPTUREFOLDER}/Run${run}/mpath.log
	echo -e "\n" > ${CAPTUREFOLDER}/Run${run}/station.log
	while true
	do
		echo -e "\n$time_val ----------------------------------------\n" >> ${CAPTUREFOLDER}/Run${run}/mpath.log
		echo -e "\n$time_val ----------------------------------------\n" >> ${CAPTUREFOLDER}/Run${run}/station.log
		iw dev wlan0 mpath dump >> ${CAPTUREFOLDER}/Run${run}/mpath.log
		iw dev wlan0 station dump >> ${CAPTUREFOLDER}/Run${run}/station.log
		time_val=$(($time_val+1))
		if [[ $time_val -gt $IPERFDURATION ]]; then
			break
		fi
	sleep 1
	done
}

for i in $(seq $RUN 1 $(($RUN + $RUNS - 1))); do

	echo 3 > /proc/sys/vm/drop_caches

	mkdir -p ${CAPTUREFOLDER}/Run${i}
	IPERFLOGFILE="${CAPTUREFOLDER}/Run${i}/iperf.log"

	# STA/MPATH logging
	sta_mpath_logging $i &
	pid=$!

	# iperf 3 server starten
	# -s Server
	# -1 nach einer Verbindung beenden
	# -J Ausgabe im JSON Format
	# -V more verbose
	# -I PID File erzeugen (+ Pfad)
	# -i Ausgabe Intervall
	# --logfile logfile erzeugen (+ Pfad)
	#
	#timeout $IPERFTIMEOUT iperf3 -s -1 -J -V -I $IPERFPIDFILE -i 0.1 --logfile $IPERFLOGFILE
	iperf3 -s $IPERFSERVERIP -1 -V -i 1 --logfile $IPERFLOGFILE

	kill -9 $pid

	# Angefallene Dateien verschieben
	if [[ "x${TCP}" == "xtcp" ]]; then
		destinationfolder="${DSTROOT}/HT${HT}_MCS${MCS}_TCP"
	else
		destinationfolder="${DSTROOT}/HT${HT}_MCS${MCS}_UDP-${TCP}"
	fi

	FILELIST=$(ls -d $CAPTUREFOLDER/Run${i}/*.log)
	for file_ in $FILELIST; do
		mkdir -p ${destinationfolder}/Run${i}
		mv ${file_} ${destinationfolder}/Run${i}/SERVER_$(basename $file_)
	done
	rmdir $CAPTUREFOLDER/Run${i}

done

