#!/bin/bash

DSTROOT="${PWD}/Messungen"
CAPTUREFOLDER="/tmp"
IPERFLOGFILE="${CAPTUREFOLDER}/iperf.log"
IPERFCLIENTIP="192.168.0.40"
IPERFSERVERIP="192.168.0.48"
IPERFTIMEOUT=100s

TCP=$1	# entweder: "tcp" oder "1M", "2M", "14.5M"
RUN=$2	# Messnummer

# Vorbereitungen treffen
ifconfig eth0 up
ip -4 addr add $IPERFSERVERIP/24 broadcast 192.168.0.255 dev eth0
echo 3 > /proc/sys/vm/drop_caches

# iperf 3 server starten
# -s Server
# -1 nach einer Verbindung beenden
# -J Ausgabe im JSON Format
# -V more verbose
# -I PID File erzeugen (+ Pfad)
# -i Ausgabe Intervall
# --logfile logfile erzeugen (+ Pfad)
#
#timeout $IPERFTIMEOUT iperf3 -s -1 -J -V -I $IPERFPIDFILE -i 0.1 --logfile $IPERFLOGFILE
timeout $IPERFTIMEOUT iperf3 -s $IPERFSERVERIP -1 -V -i 1 --logfile $IPERFLOGFILE

sleep 5s

# Angefallene Dateien verschieben
if [[ "x${TCP}" == "xtcp" ]]; then
	destinationfolder="${DSTROOT}/ETH_TCP" 
else
	destinationfolder="${DSTROOT}/ETH_UDP-${TCP}" 
fi

FILELIST=$(ls -d $CAPTUREFOLDER/*.log $CAPTUREFOLDER/*.dat)
for file_ in $FILELIST; do
	mkdir -p ${destinationfolder}/Run${RUN}/
	mv ${file_} ${destinationfolder}/Run${RUN}/SERVER_$(basename $file_)
done

