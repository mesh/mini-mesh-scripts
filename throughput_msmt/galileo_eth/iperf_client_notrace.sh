#!/bin/bash

DSTROOT="${PWD}/Messungen"
CAPTUREFOLDER="/tmp"
IPERFLOGFILE="${CAPTUREFOLDER}/iperf.log"
IPERFCLIENTIP="192.168.0.40"
IPERFSERVERIP="192.168.0.48"
IPERFTIMEOUT=100s
IPERFDURATION=5 # 60 # transmission duration in seconds

TCP=$1	# entweder: "tcp" oder "1M", "2M", "14.5M"
RUN=$2	# Messnummer

# Vorbereitungen treffen
ifconfig eth0 up
ip -4 addr add $IPERFCLIENTIP/24 broadcast 192.168.0.255 dev eth0
echo 3 > /proc/sys/vm/drop_caches

sleep 5s

# iperf 3 client starten
# -n Datenmenge	# -t Zeit # -k Anzahl Packets (Blocks)
# -c server IP
# --cport verwendeter TCP port 
# --bind an das entsprechende interface binden (ueber IP Adresse)
# -i log interval
# -J Ausgabe im JSON Format
# -V more verbose
# --logfile Pfad zum log
# -w TCP window or UDP socket buffer
# -l length of r/w buffer (default: 128kB TCP / 1448 bytes UDP; max 65507 bytes UDP)
# -Z use zerocopy API
# -O omit certain number of initial seconds for calculating results
# -N set nodelay option (disable Nagle)
# -S set ToS class
# -C set TCP cong. control alg.
#
#timeout $IPERFTIMEOUT iperf3 -c $IPERFSERVERIP -n 30M --cport $CLIENT_IPERF_PORT --bind $(iam ip) -i 0.1 -J -V --logfile $IPERFLOGFILE
if [[ "x${TCP}" == "xtcp" ]]; then
	timeout $IPERFTIMEOUT iperf3 -c $IPERFSERVERIP -t $IPERFDURATION -i 1 -V --logfile $IPERFLOGFILE -Z -O 2 # -w 1M # -w 2M
else
	timeout $IPERFTIMEOUT iperf3 -c $IPERFSERVERIP -t $IPERFDURATION -u -b $udp -i 1 -V --logfile $IPERFLOGFILE -Z -O 2 -w 1M # -w 2M # -w 4M # -l 65507
fi

sleep 5s

# Angefallene Dateien verschieben
if [[ "x${TCP}" == "xtcp" ]]; then
	destinationfolder="${DSTROOT}/ETH_TCP" 
else
	destinationfolder="${DSTROOT}/ETH_UDP-${TCP}" 
fi

FILELIST=$(ls -d $CAPTUREFOLDER/*.log $CAPTUREFOLDER/*.dat)
for file_ in $FILELIST; do
	mkdir -p ${destinationfolder}/Run${RUN}/
	mv ${file_} ${destinationfolder}/Run${RUN}/CLIENT_$(basename $file_)
done

