inPathTable = False

fin = open("SERVER_mpath.log", "r")
fout = open("SERVER_mpath_ALMs.txt", "w")

for line in fin:

    #print(line)

    if (inPathTable): # check path table entries

        if (line == "\n"):
            inPathTable = False
            continue
        
        words = str.split(line)
        print(words)
        
        if (words[0] == "04:f0:21:10:c3:52"): # we found the path to SERVER
            
            if (words[0] != words[1]): # multi-hop path
              alm = words[4]
              #print("ALM: %s" % alm)
              fout.write(alm + "\n")
              # TODO: write to output file as new line
              inPathTable = False
              
            else:
              fout.write("\n")

    if "METRIC" in line: # path table entries are following now
        #print("inPathTable")
        inPathTable = True

fin.close()
fout.close()
