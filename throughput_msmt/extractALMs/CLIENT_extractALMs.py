inPathTable = False

fin = open("CLIENT_mpath.log", "r")
fout = open("CLIENT_mpath_ALMs.txt", "w")

for line in fin:

    #print(line)

    if (inPathTable): # check path table entries

        if (line == "\n"):
            inPathTable = False
            continue
        
        words = str.split(line)
        print(words)
        
        if (words[0] == "04:f0:21:10:c8:0e"): # we found the path to SERVER
            alm = words[4]
            #print("ALM: %s" % alm)
            fout.write(alm + "\n")
            # TODO: write to output file as new line
            inPathTable = False

    if "METRIC" in line: # path table entries are following now
        #print("inPathTable")
        inPathTable = True

fin.close()
fout.close()
