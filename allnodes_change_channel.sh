#!/bin/bash

LIST=$(seq 1 1 36)
#LIST=$(seq 1 1 40)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29" # 31 32 33 34 35"

for i in $LIST ; do
	
#	ssh root@"galileo$i" "ifconfig wlan0 down &&  \
#				 iw dev wlan0 set txpower fixed 1200 && iwconfig && \
#				 ifconfig wlan0 up && \
#				 iw dev wlan0 mesh join mesh freq 5745 HT20 basic-rates 24 mcast-rate 24 && \
#				 iw dev wlan0 set bitrates ht-mcs-5 3 lgi-5" &

	ssh root@"galileo$i" "iw dev wlan0 mesh join mesh freq 5745 HT20 basic-rates 24 mcast-rate 24 && \
				 iw dev wlan0 set bitrates ht-mcs-5 9 lgi-5 \
				 iw dev wlan0 set txpower fixed 1200 && iwconfig" &

done

