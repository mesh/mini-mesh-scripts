#!/bin/bash

LIST=$(seq 1 1 40)

for i in $LIST ; do

	echo "Copying to Galileo $i ..."
	
	# Ralink USB dongles (Buffalo, ASUS, CSL)
	rsync -a deploy/firmware-ralink/rt2870.bin_0.29 root@"galileo$i":/lib/firmware/ &	# v29 (old)
	rsync -a deploy/firmware-ralink/rt2870.bin_0.36 root@"galileo$i":/lib/firmware/ &	# v36 (current)
	rsync -a deploy/firmware-ralink/rt2870.bin root@"galileo$i":/lib/firmware/ &		# renamed v36

	# Ralink mPCIe cards
	rsync -a deploy/firmware-ralink/rt2860.bin_0.34 root@"galileo$i":/lib/firmware/ &	# v34 (old)
	rsync -a deploy/firmware-ralink/rt2860.bin_0.40 root@"galileo$i":/lib/firmware/ &	# v40 (current)
	rsync -a deploy/firmware-ralink/rt2860.bin root@"galileo$i":/lib/firmware/ &		# renamed v40

done

