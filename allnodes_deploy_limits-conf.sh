#!/bin/bash

#LIST=$(seq 1 1 40)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"	# 5x5 grid

for i in $LIST ; do

	echo "Copying to Galileo $i ..."
	
	# TODO: increased open file limits
	rsync -a deploy/limits/sysctl.conf root@"galileo$i":/etc/ &
	rsync -a deploy/limits/system.conf root@"galileo$i":/etc/systemd/ &
	rsync -a deploy/limits/user.conf root@"galileo$i":/etc/systemd/ &
	rsync -a deploy/limits/limits.conf root@"galileo$i":/etc/security/ &	# actually unused with systemd

	# TODO: defaults
#	rsync -a deploy/limits/defaults/sysctl.conf root@"galileo$i":/etc/ &
#	rsync -a deploy/limits/defaults/system.conf root@"galileo$i":/etc/systemd/ &
#	rsync -a deploy/limits/defaults/user.conf root@"galileo$i":/etc/systemd/ &
#	rsync -a deploy/limits/defaults/limits.conf root@"galileo$i":/etc/security/ &	# actually unused with systemd

done
