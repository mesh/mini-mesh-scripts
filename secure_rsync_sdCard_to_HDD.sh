#!/usr/bin/env bash

# first mount both SD card partitions (BOOTFS, ROOTFS) manually!
BOOTFS="/media/$USER/INTENSO"
ROOTFS="/media/$USER/b3180269-e5af-43e0-8b6e-fa5dd4fd075d"

# target folders on HDD
GALILEO_FILE_FOLDER_BOOT="/home/$USER/Galileo_BOOT"
GALILEO_FILE_FOLDER_ROOT="/home/$USER/Galileo_ROOT"

sudo rsync -av --delete $BOOTFS/* $GALILEO_FILE_FOLDER_BOOT
sudo rsync -av --delete $ROOTFS/* $GALILEO_FILE_FOLDER_ROOT
