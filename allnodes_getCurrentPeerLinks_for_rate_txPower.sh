#!/bin/bash

FILEPATH_LOGS_STATS=/home/mret/Arbeitsfläche/peerLinks
mkdir $FILEPATH_LOGS_STATS

NODES="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"		# 5x5 grid (full)
POWERS="3 4 5 6 7 8 9 10 11 12 13 14 15 16 17"						# in dBm

BMCRATE=24
MCS=3

#BMCRATE=36
#MCS=4


for i in $NODES ; do

	ssh root@"galileo$i"	"\
			iw dev wlan0 mesh leave && \
			iw dev wlan0 mesh join mesh freq 5745 HT20 basic-rates $BMCRATE mcast-rate $BMCRATE && \
			iw dev wlan0 set bitrates ht-mcs-5 $MCS lgi-5 && \
			iw dev wlan0 set txpower fixed 300 \
			" &

done

sleep 10

for j in $POWERS ; do

	POWER=$(($j*100))

	for k in $NODES ; do

		ssh root@"galileo$k" "iw dev wlan0 set txpower fixed $POWER && iwconfig wlan0 | grep Tx-Power" &

	done

	sleep 20

	./allnodes_getCurrentPeerLinks.sh > $FILEPATH_LOGS_STATS/peerLinks-MCS$MCS-txPower$POWER.log

done

