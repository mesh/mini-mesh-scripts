#!/bin/bash

LIST=${@:-"$(seq 1 1 36)"}		#Default Galileo1,Galileo2,...,Galileo36
					#ODER Commandline Nummern also: 'rsync_test.sh 1 2 7 9'


echo $(date +%R" "%F) > date.log
for i in $LIST ; do
	$(rsync date.log root@galileo$i:/home/root/) && echo -e "Galileo${i}\tErfolgreich" || echo -e "Galileo${i}\tFehler!" &
done
