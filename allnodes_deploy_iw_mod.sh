#!/bin/bash

#LIST=$(seq 1 1 40)
#LIST=$(seq 1 1 36)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

for i in $LIST ; do
	
	echo "Copying to Galileo $i ..."

	rsync -a deploy/iw_mod/bin_c/iw_mod root@galileo$i:/home/root/CHaChA_distributed/exec/ &
	rsync -a deploy/iw_mod/bin_c/iw_mod root@galileo$i:/home/root/Azureus/bin_c/ &
	rsync -a deploy/iw_mod/bin_c/iw_mod root@galileo$i:/home/root/MeshNode/bin_c/ &
	rsync -a deploy/iw_mod/bin_c/iw_mod root@galileo$i:/home/root/kadmesh/ &

done
