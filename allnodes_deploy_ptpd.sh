#!/bin/bash

#LIST=$(seq 1 1 36)
LIST=$(seq 1 1 40)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29" # 31 32 33 34 35"

for i in $LIST ; do
	
	echo "Copying to Galileo $i ..."

	scp deploy/install_ptpd.sh root@galileo$i:/home/root/install_ptpd.sh
	scp deploy/ptpd-2.3.1.tar.gz root@galileo$i:/home/root/galileo_mesh_userspace-tools/ptpd-2.3.1.tar.gz
	scp deploy/ptp_master.conf root@galileo$i:/home/root/ptp_master.conf
	scp deploy/ptp_slave.conf root@galileo$i:/home/root/ptp_slave.conf

done

