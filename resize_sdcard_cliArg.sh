#!/bin/bash

DEVICE="$1"

resize_partition() {
  sudo umount $DEVICE*
  sudo fdisk $DEVICE <<EOF
d
2
n
2

+7G
w
EOF
  sudo e2fsck -f ${DEVICE}2
  sudo resize2fs ${DEVICE}2
  echo -e "\n\nFertig"
}

resize_partition

