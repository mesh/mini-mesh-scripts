#!/bin/sh

set -x

ifconfig wlan0 down
iw dev wlan0 del
iw phy0 interface add mon0 type monitor
ifconfig mon0 up

iw dev mon0 set channel 36
#iw dev mon0 set channel 149
#iw dev mon0 set channel 161
#iw dev mon0 set channel 165

