#!/bin/bash

#LIST=$(seq 1 1 40)
LIST=$(seq 1 1 36)

for i in $LIST ; do

	echo "Copying to Galileo $i ..."
	
	scp -rp deploy/address_list.sh root@"galileo$i":/home/root &
	scp -rp deploy/phyNameDetectAndDevRename.sh root@"galileo$i":/home/root &
	scp -rp deploy/iam root@"galileo$i":/bin/iam &

done

