#!/bin/bash

GALILEO_FILE_FOLDER_BOOT="/home/$USER/Galileo_BOOT"
GALILEO_FILE_FOLDER_ROOT="/home/$USER/Galileo_ROOT"

LOG_FOLDER="${PWD}/log"

CARD_NAME="INTENSO"

create_new_partition() {
  card="$1"
  sudo umount ${card}1
  sudo umount ${card}2
  sudo fdisk $card <<EOF
g
n


+100M
n


+7G
w
EOF
}

format_new_partition() {
  partition1="${1}1"
  partition2="${1}2"
  sudo mkfs.vfat -F 32 -n ${CARD_NAME} ${partition1}
  sudo mkfs.ext2 $partition2 <<EOF
j
EOF
  sudo tune2fs -U b3180269-e5af-43e0-8b6e-fa5dd4fd075d $partition2
}

mount_new_partition() {
  card=$1
  mount_point_ending=${card#"/dev/sd"}

  sudo mkdir /media/$USER/Card_${mount_point_ending}_1
  sudo mkdir /media/$USER/Card_${mount_point_ending}_2

  sudo mount ${card}1 /media/$USER/Card_${mount_point_ending}_1
  sudo mount ${card}2 /media/$USER/Card_${mount_point_ending}_2
}

rsync_galileo_files() {
  card=$1
  mount_point_ending=${card#"/dev/sd"}

  sudo rsync -av --progress ${GALILEO_FILE_FOLDER_BOOT}/* /media/$USER/Card_${mount_point_ending}_1
  sudo rsync -av --progress ${GALILEO_FILE_FOLDER_ROOT}/* /media/$USER/Card_${mount_point_ending}_2
}

unmount_new_partition() {
  card=$1
  mount_point_ending=${card#"/dev/sd"}

  sudo umount /media/$USER/Card_${mount_point_ending}_1
  sudo umount /media/$USER/Card_${mount_point_ending}_2

  sudo rm -rf /media/$USER/Card_${mount_point_ending}_1
  sudo rm -rf /media/$USER/Card_${mount_point_ending}_2
}

do_all() {
  device="$1"
  create_new_partition $device
  format_new_partition $device
  mount_new_partition $device
  rsync_galileo_files $device
  unmount_new_partition $device
  echo "$1 is done"
}

# get devices with fat32(-like) UUID
mem_list=$(sudo blkid | grep -E "UUID=\"[[:alnum:]]{4}-[[:alnum:]]{4}\"" | grep -o "/dev/sd[b-z]")
#mem_list="/dev/sdb /dev/sdc /dev/sdd /dev/sde /dev/sdf /dev/sdg /dev/sdh /dev/sdi /dev/sdj /dev/sdk /dev/sdl /dev/sdm /dev/sdn"

# unmount partitions
sudo umount /dev/sd[b-z]*

# log file handling
mkdir ${LOG_FOLDER}
rm "${LOG_FOLDER}/*.log"

# write data to devices
for i in $mem_list; do
    echo $i > "${LOG_FOLDER}/STDOUT_STDERR_$(basename $i).log"
    do_all "$i" > "${LOG_FOLDER}/STDOUT_STDERR_$(basename $i).log" 2>&1 &
done

