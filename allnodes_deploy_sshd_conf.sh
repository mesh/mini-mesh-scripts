#!/bin/bash

#LIST=$(seq 1 1 36)
#LIST=$(seq 1 1 40)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

for i in $LIST ; do
	
	echo "Copying to Galileo $i ..."
	
	scp deploy/ssh_galileo/sshd_config root@"galileo$i":/etc/ssh/sshd_config &
#	scp deploy/ssh_galileo/sshenv root@"galileo$i":/home/root/.ssh/environment &
#	scp deploy/ssh_galileo/authorized_keys root@"galileo$i":/home/root/.ssh/authorized_keys &

done

sleep 5

for i in $LIST ; do
	
	echo "Restarting ssh service on Galileo $i ..."

	ssh root@"galileo$i" "service ssh restart" &

done

#sleep 5

#for i in $LIST ; do
	
#	ssh root@"galileo$i" "cat /etc/ssh/sshd_config | grep MaxStartups" &

#done

