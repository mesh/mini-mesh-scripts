#!/bin/bash

file="/usr/bin/perf"

#LIST=$(seq 1 1 36)
LIST=$(seq 1 1 40)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29" # 31 32 33 34 35"

for i in $LIST ; do
	
	echo
	echo "Checking perf on Galileo $i ..."

	if ssh root@galileo$i test -e $file;
	    then echo $file exists
	    else echo $file does not exist
	fi

	echo

done

