#!/bin/bash

# all devices
LIST=$(seq 1 1 40)
#LIST="1 9 10 16"
#LIST="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40"

# grid 5x5 (MeNTor, CHaChA, KadMesh)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

# grid 6x6 (...)
#LIST=$(seq 1 1 36)
#LIST="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"

# 5- to 10-hop path (MARCo)
#LIST="6 12 18 24 30 36"
#LIST="6 12 18 24 30 36 35 34 33 32 31"


for i in $LIST ; do

	ip=$(($i+9))

	#sshpass -p meshman11s ssh -o StrictHostKeyChecking=no root@"galileo$i" "rm -r /etc/ssh/authorized_keys"
	#sshpass -p meshman11s ssh -o StrictHostKeyChecking=no root@"galileo$i" "rm -r /home/root/.ssh/authorized_keys"
	#cat $HOME/.ssh/id_rsa.pub | sshpass -p meshman11s ssh -o StrictHostKeyChecking=no root@"galileo$i" "umask 077; mkdir /home/root/.ssh; cat >> /home/root/.ssh/authorized_keys"

	ssh-keygen -f "$HOME/.ssh/known_hosts" -R "galileo$i"
	ssh-keygen -f "$HOME/.ssh/known_hosts" -R "192.168.0.$ip"

	ssh -o StrictHostKeyChecking=no root@"galileo$i" ":"
	ssh -o StrictHostKeyChecking=no root@"192.168.0.$ip" ":"

done

