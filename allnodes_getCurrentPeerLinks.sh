#!/bin/bash

LIST=$(seq 1 1 36)
#LIST=$(seq 1 1 40)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"
#LIST="6 12 18 24 30 36" # 35 34 33 32 31"

TMPFOLDER=$HOME/tmp_peerLinks

rm -r $TMPFOLDER 2> /dev/null
mkdir $TMPFOLDER

for i in $LIST ; do

	ssh root@"galileo$i" "/home/root/getCurrentPeerLinksUnfiltered.sh" > $TMPFOLDER/$i.log &
#	ssh root@"galileo$i" "/home/root/getCurrentPeerLinksFiltered.sh" > $TMPFOLDER/$i.log &

done

sleep 10

for i in $LIST ; do

	cat $TMPFOLDER/$i.log >> $TMPFOLDER/merge.log

done

cat $TMPFOLDER/merge.log

