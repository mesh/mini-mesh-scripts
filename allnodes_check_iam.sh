#!/bin/bash

LIST=${@:-"$(seq 1 1 40)"}		#Default Galileo1,Galileo2,...,Galileo36
								#ODER Commandline Nummern also: 'rsync_test.sh 1 2 7 9'
for i in $LIST ; do

	echo "Galileo${i}"
	ssh root@galileo$i "/bin/iam"
	echo ""

done

