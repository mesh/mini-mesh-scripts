#!/bin/bash

#LIST=$(seq 1 1 36)
#LIST=$(seq 1 1 40)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"
#LIST="6 12 18 24 30 36" # 35 34 33 32 31"

TMPFOLDER=$HOME/tmp_noiseFloor

rm -r $TMPFOLDER 2> /dev/null
mkdir $TMPFOLDER

for i in $LIST; do

#	ssh root@"galileo$i" "var=\$(iam && echo \":\" && cat /sys/kernel/debug/ieee80211/phy0/ath9k/nf_override) && echo \$var" &
	ssh root@"galileo$i" "var=\$(cat /sys/kernel/debug/ieee80211/phy0/ath9k/nf_override) && echo \$var" > $TMPFOLDER/$i.log &

done

sleep 10

for i in $LIST ; do

	echo "NF_G$i=$(cat $TMPFOLDER/$i.log)" >> $TMPFOLDER/merge.log

done

cat $TMPFOLDER/merge.log
