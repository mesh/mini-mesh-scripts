#!/bin/bash

#set -x

source /home/root/phyNameDetectAndDevRename.sh

rm /home/root/calibNF_* 2>/dev/null

echo $$>/home/root/calibNF_pid.tmp

LOGFILE=/home/root/calibNF_$(iam).log
DONEFILE=/home/root/calibNF_$(iam).done

NEIGHBORS=" "

# get neighborhood	#TODO: set desired neighbors based on testbed mode (6x6, 5x5, MARCo short/long)
case $(iam) in

"Galileo1") # Galileo 1
	NEIGHBORS="Galileo2 Galileo7 Galileo8"														# 6x6, 5x5
	;;
"Galileo2") # Galileo 2
	NEIGHBORS="Galileo1 Galileo3 Galileo7 Galileo8 Galileo9"									# 6x6, 5x5
	;;
"Galileo3") # Galileo 3
	NEIGHBORS="Galileo2 Galileo4 Galileo8 Galileo9 Galileo10"									# 6x6, 5x5
	;;
"Galileo4") # Galileo 4
	NEIGHBORS="Galileo3 Galileo5 Galileo9 Galileo10 Galileo11"									# 6x6, 5x5
	;;
"Galileo5") # Galileo 5
	NEIGHBORS="Galileo4 Galileo6 Galileo10 Galileo11 Galileo12"									# 6x6
	NEIGHBORS="Galileo4 Galileo10 Galileo11"													# 5x5
	;;
"Galileo6") # Galileo 6
	NEIGHBORS="Galileo5 Galileo11 Galileo12"													# 6x6
	NEIGHBORS="Galileo12"																		# MARCo (short, long)
	;;
"Galileo7") # Galileo 7
	NEIGHBORS="Galileo1 Galileo2 Galileo8 Galileo13 Galileo14"									# 6x6, 5x5
	;;
"Galileo8") # Galileo 8
	NEIGHBORS="Galileo1 Galileo2 Galileo3 Galileo7 Galileo9 Galileo13 Galileo14 Galileo15"		# 6x6, 5x5
	;;
"Galileo9") # Galileo 9
	NEIGHBORS="Galileo2 Galileo3 Galileo4 Galileo8 Galileo10 Galileo14 Galileo15 Galileo16"		# 6x6, 5x5
	;;
"Galileo10") # Galileo 10
	NEIGHBORS="Galileo3 Galileo4 Galileo5 Galileo9 Galileo11 Galileo15 Galileo16 Galileo17"		# 6x6, 5x5
	;;
"Galileo11") # Galileo 11
	NEIGHBORS="Galileo4 Galileo5 Galileo6 Galileo10 Galileo12 Galileo16 Galileo17 Galileo18"	# 6x6
	NEIGHBORS="Galileo4 Galileo5 Galileo10 Galileo16 Galileo17"									# 5x5
	;;
"Galileo12") # Galileo 12
	NEIGHBORS="Galileo5 Galileo6 Galileo11 Galileo17 Galileo18"									# 6x6
	NEIGHBORS="Galileo6 Galileo18"																# MARCo (short, long)
	;;
"Galileo13") # Galileo 13
	NEIGHBORS="Galileo7 Galileo8 Galileo14 Galileo19 Galileo20"									# 6x6, 5x5
	;;
"Galileo14") # Galileo 14
	NEIGHBORS="Galileo7 Galileo8 Galileo9 Galileo13 Galileo15 Galileo19 Galileo20 Galileo21"	# 6x6, 5x5
	;;
"Galileo15") # Galileo 15
	NEIGHBORS="Galileo8 Galileo9 Galileo10 Galileo14 Galileo16 Galileo20 Galileo21 Galileo22"	# 6x6, 5x5
	;;
"Galileo16") # Galileo 16
	NEIGHBORS="Galileo9 Galileo10 Galileo11 Galileo15 Galileo17 Galileo21 Galileo22 Galileo23"	# 6x6, 5x5
	;;
"Galileo17") # Galileo 17
	NEIGHBORS="Galileo10 Galileo11 Galileo12 Galileo16 Galileo18 Galileo22 Galileo23 Galileo24"	# 6x6
	NEIGHBORS="Galileo10 Galileo11 Galileo16 Galileo22 Galileo23"								# 5x5
	;;
"Galileo18") # Galileo 18
	NEIGHBORS="Galileo11 Galileo12 Galileo17 Galileo23 Galileo24"								# 6x6
	NEIGHBORS="Galileo12 Galileo24"																# MARCo (short, long)
	;;
"Galileo19") # Galileo 19
	NEIGHBORS="Galileo13 Galileo14 Galileo20 Galileo25 Galileo26"								# 6x6, 5x5
	;;
"Galileo20") # Galileo 20
	NEIGHBORS="Galileo13 Galileo14 Galileo15 Galileo19 Galileo21 Galileo25 Galileo26 Galileo27"	# 6x6, 5x5
	;;
"Galileo21") # Galileo 21
	NEIGHBORS="Galileo14 Galileo15 Galileo16 Galileo20 Galileo22 Galileo26 Galileo27 Galileo28"	# 6x6, 5x5
	;;
"Galileo22") # Galileo 22
	NEIGHBORS="Galileo15 Galileo16 Galileo17 Galileo21 Galileo23 Galileo27 Galileo28 Galileo29"	# 6x6, 5x5
	;;
"Galileo23") # Galileo 23
	NEIGHBORS="Galileo16 Galileo17 Galileo18 Galileo22 Galileo24 Galileo28 Galileo29 Galileo30"	# 6x6
	NEIGHBORS="Galileo16 Galileo17 Galileo22 Galileo28 Galileo29"								# 5x5
	;;
"Galileo24") # Galileo 24
	NEIGHBORS="Galileo17 Galileo18 Galileo23 Galileo29 Galileo30"								# 6x6
	NEIGHBORS="Galileo18 Galileo30"																# MARCo (short, long)
	;;
"Galileo25") # Galileo 25
	NEIGHBORS="Galileo19 Galileo20 Galileo26 Galileo31 Galileo33"								# 6x6
	NEIGHBORS="Galileo19 Galileo20 Galileo26"													# 5x5
	;;
"Galileo26") # Galileo 26
	NEIGHBORS="Galileo19 Galileo20 Galileo21 Galileo25 Galileo27 Galileo31 Galileo32 Galileo33"	# 6x6
	NEIGHBORS="Galileo19 Galileo20 Galileo21 Galileo25 Galileo27"								# 5x5
	;;
"Galileo27") # Galileo 27
	NEIGHBORS="Galileo20 Galileo21 Galileo22 Galileo26 Galileo28 Galileo32 Galileo33 Galileo34"	# 6x6
	NEIGHBORS="Galileo20 Galileo21 Galileo22 Galileo26 Galileo28"								# 5x5
	;;
"Galileo28") # Galileo 28
	NEIGHBORS="Galileo21 Galileo22 Galileo23 Galileo27 Galileo29 Galileo33 Galileo34 Galileo35"	# 6x6
	NEIGHBORS="Galileo21 Galileo22 Galileo23 Galileo27 Galileo29"								# 5x5
	;;
"Galileo29") # Galileo 29
	NEIGHBORS="Galileo22 Galileo23 Galileo24 Galileo28 Galileo30 Galileo34 Galileo35 Galileo36"	# 6x6
	NEIGHBORS="Galileo22 Galileo23 Galileo28"													# 5x5
	;;
"Galileo30") # Galileo 30
#	NEIGHBORS="Galileo23 Galileo24 Galileo29 Galileo35 Galileo36"								# 6x6
	NEIGHBORS="Galileo24 Galileo36"																# MARCo short
#	NEIGHBORS="Galileo24 Galileo35"																# MARCo long diag
#	NEIGHBORS="Galileo24 Galileo36"																# MARCo long corner
	;;
"Galileo31") # Galileo 31
	NEIGHBORS="Galileo25 Galileo26 Galileo32"													# 6x6
#	NEIGHBORS="Galileo32"																		# MARCo long
	;;
"Galileo32") # Galileo 32
	NEIGHBORS="Galileo25 Galileo26 Galileo27 Galileo31 Galileo33"								# 6x6
#	NEIGHBORS="Galileo31 Galileo33"																# MARCo long
	;;
"Galileo33") # Galileo 33
	NEIGHBORS="Galileo26 Galileo27 Galileo28 Galileo32 Galileo34"								# 6x6
#	NEIGHBORS="Galileo32 Galileo34"																# MARCo long
	;;
"Galileo34") # Galileo 34
	NEIGHBORS="Galileo27 Galileo28 Galileo29 Galileo33 Galileo35"								# 6x6
#	NEIGHBORS="Galileo33 Galileo35"																# MARCo long
	;;
"Galileo35") # Galileo 35
	NEIGHBORS="Galileo28 Galileo29 Galileo30 Galileo34 Galileo36"								# 6x6
#	NEIGHBORS="Galileo34 Galileo30"																# MARCo long diag
#	NEIGHBORS="Galileo34 Galileo36"																# MARCo long corner
	;;
"Galileo36") # Galileo 36
	NEIGHBORS="Galileo29 Galileo30 Galileo35"													# 6x6
	NEIGHBORS="Galileo30"																		# MARCo short
#	NEIGHBORS="Galileo30 Galileo35"																# MARCo long corner
	;;
#-------------------------------------------------------------------------------
"Galileo37") # Galileo 37		test node, MARCo interferer sender
	NEIGHBORS="Galileo38"
	;;
"Galileo38") # Galileo 38		test node, MARCo interferer receiver
	NEIGHBORS="Galileo37"
	;;
"Galileo39") # Galileo 39		test node
	NEIGHBORS="Galileo40"
	;;
"Galileo40") # Galileo 40		test node
	NEIGHBORS="Galileo39"
	;;
*)
	;;
esac

NEIGHBORS_ARRAY=($NEIGHBORS)
NEIGHBORCOUNT=${#NEIGHBORS_ARRAY[@]}

NF_OVERRIDE_PATH=/sys/kernel/debug/ieee80211/${PHYNAME_ATH}/ath9k/nf_override

NF_SETTLE_TIME=10					# TODO: settle time in seconds

# TODO: starting condition: at least all desired links are present at $NF_MIN
NF_MIN=-120
NF_MAX=-95

# TODO: NF_INIT
NF_INIT=$NF_MIN						# start at minimum NF
#NF_INIT=$(cat $NF_OVERRIDE_PATH)	# start with current NF
#NF_INIT=-115						# start at certain fixed value

NF_CURRENT=$NF_INIT

echo $NF_CURRENT > ${NF_OVERRIDE_PATH} # apply initial NF

FOUND_ALL_ONCE=false
LOST_DESIRED_ONCE=false

while (($NF_CURRENT <= $NF_MAX)) && (($NF_CURRENT >= $NF_MIN)) ; do

	sleep $NF_SETTLE_TIME # wait for NF override to have effect on links

	echo $NF_CURRENT >> $LOGFILE

	echo $(lsof -w -p $BASHPID | wc -l) open files >> $LOGFILE

	# parse current neighbor names to array (ignore first to lines -> \n and iam)
#	./getCurrentPeerLinksUnfiltered.sh > peerLinks.temp && : || echo "getCurrentPeerLinks did not exit" >> $LOGFILE
	./getCurrentPeerLinksFiltered.sh > peerLinks.temp && : || echo "getCurrentPeerLinks did not exit" >> $LOGFILE
	LINKS=$(cat peerLinks.temp)
	LINECOUNT=$(echo "$LINKS" | wc -l)
	NEIGHBORS_CURRENT=$(echo "$LINKS" | tail -n $(($LINECOUNT-2)) | awk '{ print $1 }')
	NEIGHBORS_CURRENT_ARRAY=($NEIGHBORS_CURRENT)
	NEIGHBORCOUNT_CURRENT=${#NEIGHBORS_CURRENT_ARRAY[@]}

	echo $NEIGHBORS_CURRENT >> $LOGFILE

	# compare current links to desired links

	if (($NEIGHBORCOUNT_CURRENT >= $NEIGHBORCOUNT)); then

		# we most likely have at least the desired links and possibly some unwanted

		FOUND_ALL=true

		for node in $NEIGHBORS; do

			# check if desired $node is in current link list
			if echo $NEIGHBORS_CURRENT | grep -w $node > /dev/null; then
				
				# found desired node -> OK, continue
				:

			else

				FOUND_ALL=false
				break

			fi

		done
	
		if $FOUND_ALL; then

			FOUND_ALL_ONCE=true

			if $LOST_DESIRED_ONCE; then

				# 1 too high (had "best" result with all desired links before) -> decrement and quit
				NF_CURRENT=$(($NF_CURRENT-1))
				echo $NF_CURRENT > ${NF_OVERRIDE_PATH}
				echo went too high at $NF_CURRENT but had best result before - LOST_DESIRED_ONCE >> $LOGFILE
				echo > $DONEFILE
				exit

			fi

			# all desired links are there, check for unwanted
			if (($NEIGHBORCOUNT_CURRENT == $NEIGHBORCOUNT)); then

				# perfect -> quit
				echo perfect at $NF_CURRENT >> $LOGFILE
				echo > $DONEFILE
				exit

			else

				# NF still too low (we have all desired but unwanted links as well) -> increment by 1
				NF_CURRENT=$(($NF_CURRENT+1))
				echo $NF_CURRENT > ${NF_OVERRIDE_PATH}

			fi

		else

			if $FOUND_ALL_ONCE; then
				LOST_DESIRED_ONCE=true
			fi

			# had at least "enough" links but not all desired included -> decrement
			echo have not found all desired links - possibly went too high at $NF_CURRENT >> $LOGFILE
			if (($NF_CURRENT > $NF_MIN)); then
				NF_CURRENT=$(($NF_CURRENT-1))
				echo $NF_CURRENT > ${NF_OVERRIDE_PATH}
			else
				break
			fi
			
		fi

	else

		# 1 too high (had "best" result with all desired links before) -> decrement and quit
		NF_CURRENT=$(($NF_CURRENT-1))
		echo $NF_CURRENT > ${NF_OVERRIDE_PATH}
		echo went too high at $NF_CURRENT but had best result before >> $LOGFILE
		echo > $DONEFILE
		exit

	fi

done

echo did not succeed within NF thresholds >> $LOGFILE
echo > $DONEFILE

exit

