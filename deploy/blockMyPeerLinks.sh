#!/bin/bash

#echo $$ > /home/root/blockMyPeerLinks_pid.tmp
#echo $BASHPID > /home/root/blockMyPeerLinks_bashpid.tmp

source /home/root/address_list.sh
NODE_MESH_MAC=($NODE_MESH_MAC)

source /home/root/phyNameDetectAndDevRename.sh

myName=$(iam)
myNumber="${myName//Galileo/}"

# periodically refresh desired neighborhood
while true ; do

	case "$myNumber" in

	"1") # Galileo 1

		# open: 1 2 7 8


			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"2") # Galileo 2

		# open: 1 2 3 7 8 9


			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"3") # Galileo 3

		# open: 2 3 4 8 9 10


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"4") # Galileo 4

		# open: 3 4 5 9 10 11


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"5") # Galileo 5

		# open: 4 5 6 10 11 12


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"6") # Galileo 6

		# open: 5 6 11 12


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"7") # Galileo 7

		# open: 1 2 7 8 13 14


			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"8") # Galileo 8

		# open: 1 2 3 7 8 9 13 14 15


			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"9") # Galileo 9

		# open: 2 3 4 8 9 10 14 15 16


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"10") # Galileo 10

		# open: 3 4 5 9 10 11 15 16 17


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"11") # Galileo 11

		# open: 4 5 6 10 11 12 16 17 18


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"12") # Galileo 12

		# open: 5 6 11 12 17 18


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"13") # Galileo 13

		# open: 7 8 13 14 19 20


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"14") # Galileo 14

		# open: 7 8 9 13 14 15 19 20 21


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"15") # Galileo 15

		# open: 8 9 10 14 15 16 20 21 22


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"16") # Galileo 16

		# open: 9 10 11 15 16 17 21 22 23


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"17") # Galileo 17

		# open: 10 11 12 16 17 18 22 23 24


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"18") # Galileo 18

		# open: 11 12 17 18 23 24


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"19") # Galileo 19

		# open: 13 14 19 20 25 26


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"20") # Galileo 20

		# open: 13 14 15 19 20 21 25 26 27


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"21") # Galileo 21

		# open: 14 15 16 20 21 22 26 27 28


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"22") # Galileo 22

		# open: 15 16 17 21 22 23 27 28 29


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"23") # Galileo 23

		# open: 16 17 18 22 23 24 28 29 30


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"24") # Galileo 24

		# open: 17 18 23 24 29 30


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"25") # Galileo 25

		# open: 19 20 25 26 31 32


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"26") # Galileo 26

		# open: 19 20 21 25 26 27 31 32 33


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &
	
		;;

	"27") # Galileo 27

		# open: 20 21 22 26 27 28 32 33 34


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"28") # Galileo 28

		# open: 21 22 23 27 28 29 33 34 35


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"29") # Galileo 29

		# open: 22 23 24 28 29 30 34 35 36


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"30") # Galileo 30

		# open: 23 24 29 30 35 36


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"31") # Galileo 31

		# open: 25 26 31 32


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"32") # Galileo 32

		# open: 25 26 27 31 32 33


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"33") # Galileo 33

		# open: 26 27 28 32 33 34


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"34") # Galileo 34

		# open: 27 28 29 33 34 35


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"35") # Galileo 35

		# open: 28 29 30 34 35 36


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	"36") # Galileo 36

		# open: 29 30 35 36


			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[0]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[1]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[2]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[3]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[4]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[5]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[6]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[7]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[8]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[9]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[10]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[11]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[12]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[13]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[14]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[15]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[16]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[17]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[18]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[19]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[20]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[21]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[22]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[23]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[24]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[25]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[26]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[27]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[28]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[29]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[30]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[31]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[32]} plink_action block  &
			iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[33]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[34]} plink_action block  &
			#iw dev ${DEVNAME_ATH} station set ${NODE_MESH_MAC[35]} plink_action block  &


		;;

	*)
		;;

	esac

	sleep 30	# TODO

done


