#!/bin/bash

# ********* NF calib definitions *********
#
NF_G1=-103
NF_G2=-104
NF_G3=-105
NF_G4=-103
NF_G5=-104
#
#NF_G6=-110 #TODO
#
NF_G7=-109
NF_G8=-105
NF_G9=-107
NF_G10=-103
NF_G11=-101
#
#NF_G12=-110 #TODO
#
NF_G13=-107
NF_G14=-107
NF_G15=-106		# TODO: problematic node
NF_G16=-109
NF_G17=-112
#
#NF_G18=-110 #TODO
#
NF_G19=-109
NF_G20=-106
NF_G21=-104
NF_G22=-105		# TODO: problematic node
NF_G23=-107
#
#NF_G24=-110 #TODO
#
NF_G25=-105
NF_G26=-106
NF_G27=-105
NF_G28=-107
NF_G29=-103
#
#NF_G30=-110 #TODO
#
#NF_G31=-110 #TODO
#NF_G32=-110 #TODO
#NF_G33=-110 #TODO
#NF_G34=-110 #TODO
#NF_G35=-110 #TODO
#
#NF_G36=-110 #TODO
#
# ****************************************
#
# MARCo 5-hop
NF_G6=-110
NF_G12=-110
NF_G18=-103
NF_G24=-107
NF_G30=-110
NF_G36=-106
#
# MARCo 10-hop (currently unused)
NF_G31=-110 #TODO
NF_G32=-110 #TODO
NF_G33=-110 #TODO
NF_G34=-110 #TODO
NF_G35=-110 #TODO
#
# MARCo interferers
NF_G37=-95 #TODO
NF_G38=-95 #TODO
#
# ****************************************
#
# test nodes
NF_G39=-110 #TODO
NF_G40=-110 #TODO
#
# ****************************************
