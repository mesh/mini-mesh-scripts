
#!/bin/bash

cd /home/root/galileo_mesh_userspace-tools

rm -r ptpd-2.3.1

tar -xvzf ptpd-2.3.1.tar.gz

cd ptpd-2.3.1

./configure --disable-statistics --disable-so-timestamping

make
make install
