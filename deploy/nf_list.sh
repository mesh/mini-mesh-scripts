#!/bin/bash

# ********* NF calib definitions *********
#
NF_G1=-104		#-102
NF_G2=-104		#-107
NF_G3=-107		#-106
NF_G4=-103		#-104
NF_G5=-100		#-105
#
#NF_G6=-110 #TODO
#
NF_G7=-108		#-107
NF_G8=-107		#-107
NF_G9=-106		#-108
NF_G10=-104		#-107
NF_G11=-104		#-102
#
#NF_G12=-110 #TODO
#
NF_G13=-105		#-103
NF_G14=-106		#-106
NF_G15=-120		#-106	# TODO: problematic node
NF_G16=-108		#-101
NF_G17=-110		#-107
#
#NF_G18=-110 #TODO
#
NF_G19=-108		#-107
NF_G20=-104		#-106
NF_G21=-106		#-109
NF_G22=-120		#-106	# TODO: problematic node
NF_G23=-108		#-106
#
#NF_G24=-110 #TODO
#
NF_G25=-104		#-101
NF_G26=-102		#-103
NF_G27=-107		#-107
NF_G28=-105		#-108
NF_G29=-104		#-106
#
#NF_G30=-110 #TODO
#
#NF_G31=-110 #TODO
#NF_G32=-110 #TODO
#NF_G33=-110 #TODO
#NF_G34=-110 #TODO
#NF_G35=-110 #TODO
#
#NF_G36=-110 #TODO
#
# ****************************************
#
# MARCo 5-hop
NF_G6=-110
NF_G12=-110
NF_G18=-103
NF_G24=-107
NF_G30=-110
NF_G36=-106
#
# MARCo 10-hop (currently unused)
NF_G31=-110 #TODO
NF_G32=-110 #TODO
NF_G33=-110 #TODO
NF_G34=-110 #TODO
NF_G35=-110 #TODO
#
# MARCo interferers
NF_G37=-95 #TODO
NF_G38=-95 #TODO
#
# ****************************************
#
# test nodes
NF_G39=-110 #TODO
NF_G40=-110 #TODO
#
# ****************************************
