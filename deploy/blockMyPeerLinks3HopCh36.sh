#!/bin/bash

#set -x

source ./phyNameDetectAndDevRename.sh

# 3-Hop Test Topology: ML1 -- G39 -- G40 -- ML2

case "$1" in

"ML1")	# MeshLab 1

		#iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ef plink_action open  &	# ML1
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0e plink_action open  &	# G39
		iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action block  &	# G40
		iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ff plink_action block  &	# ML2

	;;

"G39")	# Galileo 39

		#iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ef plink_action open  &	# ML1
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0e plink_action open  &	# G39
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action open  &	# G40
		iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ff plink_action block  &	# ML2

	;;

"G40")	# Galileo 40

		iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ef plink_action block  &	# ML1
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0e plink_action open  &	# G39
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action open  &	# G40
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ff plink_action open  &	# ML2

	;;

"ML2")	# MeshLab 2

		iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ef plink_action block  &	# ML1
		iw dev ${DEVNAME_ATH} station set 04:f0:21:10:c8:0e plink_action block  &	# G39
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:14:c6:5e plink_action open  &	# G40
		#iw dev ${DEVNAME_ATH} station set 04:f0:21:26:d6:ff plink_action open  &	# ML2

	;;

*)		# default
	;;

esac
