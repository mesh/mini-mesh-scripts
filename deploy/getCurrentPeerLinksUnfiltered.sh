#!/bin/bash

source address_list.sh
source /home/root/phyNameDetectAndDevRename.sh

IFS=$'\n'	# use line feed as separator in console output to array conversion

NODE_MAC=($NODE_MAC)
NODE_MESH_IP=($NODE_MESH_IP)
NODE_NAME=($NODE_NAME)

# get local node name
localName=$(iam)

# get iw station dump output
stationDump=$(iw dev ${DEVNAME_ATH} station dump)

# get multiline output of remote stations' mac addresses
remoteMacs=($(echo "$stationDump" | grep Station | grep -io '[0-9A-F]\{2\}\(:[0-9A-F]\{2\}\)\{5\}'))
#echo -e "${remoteMacs[@]}\n"

# get multiline output of remote stations' names
shopt -s nocasematch
for i in $(seq 1 1 ${#remoteMacs[@]}); do
  for j in $(seq 1 1 ${#NODE_MAC[@]}); do
    if [[ "${remoteMacs[$(($i-1))]}" == "${NODE_MAC[$(($j-1))]}" ]]; then
        # append name belonging to MAC to "remoteNames"
		remoteNames[$(($i-1))]=${NODE_NAME[$(($j-1))]}
    fi
  done
done
shopt -u nocasematch
#echo -e "${remoteNames[@]}\n"

# get multiline output of link states
linkStates=($(echo "$stationDump" | grep plink | grep -oP '\b[A-Z_0-9]+\b.*'))
#echo -e "${linkStates[@]}\n"

# get multiline output of inactive times
inactiveTimes=($(echo "$stationDump" | grep -oP '(?<=inactive time:\t).*'))
inactiveTimesTrimmed=($(echo "$stationDump" | grep -oP '(?<=inactive time:\t).*' | awk '{ print $1 }'))
#echo -e "${inactiveTimes[@]}\n"

# get multiline output of avg receive powers
receivePowers=($(echo "$stationDump" | grep -oP '(?<=signal avg:\t).*'))
receivePowersTrimmed=($(echo "$stationDump" | grep -oP '(?<=signal avg:\t).*' | awk '{ print $1 }'))
#echo -e "${receivePowers[@]}\n"

# get multiline output of tx bitrates
txRates=($(echo "$stationDump" | grep -oP '(?<=tx bitrate:\t).*'))
#echo -e "${txRates[@]}\n"

# print reduced link table
echo -e "\n$localName"
for i in $(seq 1 1 ${#remoteNames[@]}); do

	# only print info for links with ESTAB state
	if [[ "${linkStates[$(($i-1))]}" == "ESTAB" ]]; then

		echo -e "\t${remoteNames[$(($i-1))]}\t${inactiveTimes[$(($i-1))]}\t${receivePowers[$(($i-1))]}\t${txRates[$(($i-1))]}"

    fi

done

unset IFS

exit

