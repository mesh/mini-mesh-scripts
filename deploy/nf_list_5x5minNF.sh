#!/bin/bash

# ********* NF calib definitions *********
#
NF_G1=-120
NF_G2=-120
NF_G3=-120
NF_G4=-120
NF_G5=-120
#
#NF_G6=-110 #TODO
#
NF_G7=-120
NF_G8=-120
NF_G9=-120
NF_G10=-120
NF_G11=-120
#
#NF_G12=-110 #TODO
#
NF_G13=-120
NF_G14=-120
NF_G15=-120
NF_G16=-120
NF_G17=-120
#
#NF_G18=-110 #TODO
#
NF_G19=-120
NF_G20=-120
NF_G21=-120
NF_G22=-120
NF_G23=-120
#
#NF_G24=-110 #TODO
#
NF_G25=-120
NF_G26=-120
NF_G27=-120
NF_G28=-120
NF_G29=-120
#
#NF_G30=-110 #TODO
#
#NF_G31=-110 #TODO
#NF_G32=-110 #TODO
#NF_G33=-110 #TODO
#NF_G34=-110 #TODO
#NF_G35=-110 #TODO
#
#NF_G36=-110 #TODO
#
# ****************************************
#
# MARCo 5-hop
NF_G6=-110
NF_G12=-110
NF_G18=-103
NF_G24=-107
NF_G30=-110
NF_G36=-106
#
# MARCo 10-hop (currently unused)
NF_G31=-110 #TODO
NF_G32=-110 #TODO
NF_G33=-110 #TODO
NF_G34=-110 #TODO
NF_G35=-110 #TODO
#
# MARCo interferers
NF_G37=-95 #TODO
NF_G38=-95 #TODO
#
# ****************************************
#
# test nodes
NF_G39=-110 #TODO
NF_G40=-110 #TODO
#
# ****************************************
