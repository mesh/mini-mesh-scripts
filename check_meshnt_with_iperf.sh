#!/bin/bash

PIDFILE="/tmp/check_meshnt.pid"
echo $BASHPID > $PIDFILE

IPDOMAIN="192.168.123."
TCPTEST=false
RATE="300M"

trap cleanup SIGINT SIGTERM

cleanup(){
  rm $PIDFILE
  killall $ID
  exit
}

. address_list.sh

test_connection() {

  local peer1    #ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer2} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -s -1 -f m & disown -h"
  local peer2
  local mesh_ip_peer1
  local mesh_ip_peer2

  peer1="$1"
  peer2="$2"

#TCP
# | grep \"sender\|receiver\" | grep -o \"[0-9]*.[0-9]* Mbit\" || echo "Fehler"
#UDP
# | grep -B1 " 0.00-10.0*" | grep -o "[0-9]*.[0-9]* Mbits/sec\|[0-9]*/[0-9]* ([0-9]*%)"

  mesh_ip_peer1=$(( $peer1 + 9 ))
  mesh_ip_peer2=$(( $peer2 + 9 ))

  if $TCPTEST ; then
    echo -n "${peer1}<->${peer2} TCP"

    ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer2} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -s -1 -f m > /dev/null 2>&1 & disown -h"
    sleep 1s
    conn_1_mbit=$(ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer1} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -c ${IPDOMAIN}${mesh_ip_peer2} -f m| grep \"receiver\" | grep -o \"[0-9]*.[0-9]* Mbit\" | grep -o \"[0-9]*.[0-9]* \" || echo "Fehler" & disown -h")

    sleep 3s

    ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer1} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -s -1 -f m > /dev/null 2>&1 & disown -h"
    sleep 1s
   conn_2_mbit=$(ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer2} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -c ${IPDOMAIN}${mesh_ip_peer1} -f m | grep \"receiver\" | grep -o \"[0-9]*.[0-9]* Mbit\" | grep -o \"[0-9]*.[0-9]* \" || echo "Fehler" & disown -h")

    if [[ -z $conn_1_mbit ]]; then
      echo -n " FEHLER!"
    else
      output=$(echo "$conn_1_mbit")
      echo " $output" | tr '\n' ' '
    fi
    if [[ -z $conn_2_mbit ]]; then
      echo -n " FEHLER!"
    else
      output=$(echo "$conn_2_mbit")
      echo -n " $output" | tr '\n' ' '
    fi


  else
    echo -n "${peer1}<->${peer2} UDP"

    conn_1_mbit_server=$(ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer2} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -s -1 -f m >/dev/null 2>&1 & disown -h") &
    sleep 1s
    conn_1_mbit_client=$(ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer1} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -c ${IPDOMAIN}${mesh_ip_peer2} -u -b $RATE -f m --get-server-output & disown -h")

    mbit=$(echo "$conn_1_mbit_client" | grep "0.00-10.00" | grep -o "[0-9]\{0,3\}.[0-9]\{0,2\} Mbits/sec")
    error_client=$(echo "$conn_1_mbit_client" | grep "0.00-10.00" | grep -o "[0-9]\{0,4\}/[0-9]\{1,5\}")
    error_server=$(echo "$conn_1_mbit_client" | tail -n4 | grep -o "[0-9]\{0,4\}/[0-9]\{1,5\}")

    if [[ -z $conn_1_mbit_client ]]; then
      echo -n " FEHLER!"
    else
      output=$(echo "|$mbit|$error_client|$error_server|")
      echo " $output" | tr '\n' ' '
    fi

    sleep 3s

    conn_2_mbit_server=$(ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer1} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -s -1 -f m >/dev/null 2>&1 & disown -h") &
    sleep 1s
    conn_2_mbit_client=$(ssh -y -oConnectTimeout=3 -oStrictHostKeyChecking=no root@galileo${peer2} "killall iperf3 > /dev/null 2>&1 ; timeout 20s iperf3 -c ${IPDOMAIN}${mesh_ip_peer1} -u -b $RATE -f m --get-server-output & disown -h")

    mbit=$(echo "$conn_2_mbit_client" | grep "0.00-10.00" | grep -o "[0-9]\{0,3\}.[0-9]\{0,2\} Mbits/sec")
    error_client=$(echo "$conn_2_mbit_client" | grep "0.00-10.00" | grep -o "[0-9]\{0,4\}/[0-9]\{1,5\}")
    error_server=$(echo "$conn_2_mbit_client" | tail -n4 | grep -o "[0-9]\{0,4\}/[0-9]\{1,5\}")

    if [[ -z $conn_2_mbit_client ]]; then
      echo -n " FEHLER!"
    else
      output=$(echo "|$mbit|$error_client|$error_server|")
      echo " $output" | tr '\n' ' '
    fi


  fi


}

check_all() {
  for i in $(seq 0 12 24) ; do
    for j in $(seq 1 3 6) ; do
      peer1=$(( $i + $j ))
      peer2=$(( $peer1 + 6 ))
      test_connection "$peer1" "$peer2" > conn_${peer1}-${peer2}.log &
      echo $peer1 $peer2
    done
    sleep 50s
  done

  sleep 5s

  for i in $(seq 0 12 24) ; do
    for j in $(seq 2 3 6) ; do
      peer1=$(( $i + $j ))
      peer2=$(( $peer1 + 6 ))
      test_connection "$peer1" "$peer2" > conn_${peer1}-${peer2}.log &
      echo $peer1 $peer2
    done
    sleep 50s
  done

  sleep 5s

  for i in $(seq 0 12 24) ; do
    for j in $(seq 3 3 6) ; do
      peer1=$(( $i + $j ))
      peer2=$(( $peer1 + 6 ))
      test_connection "$peer1" "$peer2" > conn_${peer1}-${peer2}.log &
      echo $peer1 $peer2
    done
    sleep 50s
  done

}


ARGV="$@"

if [[ "$1" == "all" ]] ; then
  check_all
else
  for i in $ARGV; do
    peer1=$(echo $i | cut -d"," -f1)
    peer2=$(echo $i | cut -d"," -f2)
    test_connection "$peer1" "$peer2" > conn_${peer1}-${peer2}.log &
    sleep 45s
  done
fi
