#!/bin/bash

#LIST="1 2 7 8"																	# 2x2 grid

#LIST="3 9 13 14 15"															# 3x3 grid (increment)
#LIST="1 2 3 7 8 9 13 14 15"													# 3x3 grid (full)

#LIST="4 10 16 19 20 21 22"														# 4x4 grid (increment)
#LIST="1 2 3 4 7 8 9 10 13 14 15 16 19 20 21 22"								# 4x4 grid (full)

#LIST="5 11 17 23 25 26 27 28 29"												# 5x5 grid (increment)
LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"		# 5x5 grid (full)

#LIST=$(seq 1 1 36)
#LIST=$(seq 1 1 40)

#TXPOWER=300
#TXPOWER=400
#TXPOWER=500
#TXPOWER=600
#TXPOWER=700
#TXPOWER=800
#TXPOWER=900
#TXPOWER=1000
#TXPOWER=1100
#TXPOWER=1200
#TXPOWER=1300
TXPOWER=1400
#TXPOWER=1500
#TXPOWER=1600
#TXPOWER=1700

BMCRATE=24
MCS=3

#BMCRATE=36
#MCS=4

for i in $LIST ; do

	ssh root@"galileo$i"	"\
				ifconfig wlan0 up && \
				iw dev wlan0 mesh join mesh freq 5745 HT20 basic-rates $BMCRATE mcast-rate $BMCRATE && \
				iw dev wlan0 set bitrates ht-mcs-5 $MCS lgi-5 && \
				iw dev wlan0 set mesh_param mesh_plink_timeout 20 && \
				iw dev wlan0 set txpower fixed $TXPOWER && \
				iwconfig wlan0 | grep Tx-Power \
				" &				

				#iw dev wlan0 set mesh_param mesh_max_retries 20 && \
				#iw dev wlan0 set mesh_param mesh_retry_timeout 100 && \
				#iw dev wlan0 set mesh_param mesh_confirm_timeout 100 && \
				#iw dev wlan0 set mesh_param mesh_holding_timeout 100 && \

done

