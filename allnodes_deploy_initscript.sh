#!/bin/bash

# all devices
LIST=$(seq 1 1 36)
#LIST=$(seq 1 1 40)
#LIST="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40"

# grid 5x5 (MeNTor, CHaChA, KadMesh)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

# grid 6x6
#LIST=$(seq 1 1 36)
#LIST="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36"

# 5- to 10-hop path (MARCo)
#LIST="6 12 18 24 30 36"					# 1 ..  5 hops
#LIST="35 34 33 32 31"						# 6 .. 10 hops
#LIST="6 12 18 24 30 36 35 34 33 32 31"		# 10 hops

# interferers (MARCo)
#LIST="37 38"

# test nodes
#LIST="39 40"

# ------------------------------------------------------------------------------

# grid 5x5 (MeNTor, CHaChA, KadMesh)
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"
#LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29 40" # TODO
#LIST="" # TODO

for i in $LIST ; do

	echo "Copying to Galileo $i ..."

	rsync -a deploy/address_list.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/phyNameDetectAndDevRename.sh root@"galileo$i":/home/root/ &

	# TODO: test highest CCA tolerance with minNF of -120dBm
	#rsync -a deploy/nf_list_5x5minNF.sh root@"galileo$i":/home/root/nf_list.sh &
	rsync -a deploy/nf_list_6x6minNF.sh root@"galileo$i":/home/root/nf_list.sh &
	#
	# TODO: MCS0 cfg.
	#rsync -a deploy/nf_list_5x5MCS0.sh root@"galileo$i":/home/root/nf_list.sh & # TODO
	#rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch149_MCS0 root@"galileo$i":/home/root/meshnodeinit &
	#
	# TODO: MCS3 cfg.
	#rsync -a deploy/nf_list.sh root@"galileo$i":/home/root/ & # TODO
	rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch149 root@"galileo$i":/home/root/meshnodeinit &

done

# ------------------------------------------------------------------------------

# 5- to 10-hop path (MARCo)
##LIST="6 12 18 24 30"						#  4 hops # TODO
#LIST="6 12 18 24 30 36"					#  5 hops
##LIST="35 34 33 32 31"
#LIST="6 12 18 24 30 36 35 34 33 32 31"		# 10 hops
LIST="" # TODO

for i in $LIST ; do

	echo "Copying to Galileo $i ..."

	rsync -a deploy/address_list.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/nf_list.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/phyNameDetectAndDevRename.sh root@"galileo$i":/home/root/ &

	# TODO: atheros vs. ralink setup
	#
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_defaults_ch161_atheros	root@"galileo$i":/home/root/meshnodeinit &	# TODO: theoretical defaults as backup, atheros
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_defaults_ch5_ralink	    root@"galileo$i":/home/root/meshnodeinit &	# TODO: theoretical defaults as backup, ralink
    #rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_defaults_ch13_ralink	root@"galileo$i":/home/root/meshnodeinit &	# TODO: theoretical defaults as backup, ralink
	#
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_defaults_ch161_atheros_noBASWR_noFallbackRTSCTS	root@"galileo$i":/home/root/meshnodeinit &	# TODO: used as defaults, atheros
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_defaults_ch5_ralink_noBASWR_noFallbackRTSCTS	root@"galileo$i":/home/root/meshnodeinit &	# TODO: used as defaults, ralink
    #rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_defaults_ch13_ralink_noBASWR_noFallbackRTSCTS	root@"galileo$i":/home/root/meshnodeinit &	# TODO: used as defaults, ralink
	#
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch161_atheros_limitedRateSet root@"galileo$i":/home/root/meshnodeinit &	# TODO: MCS 0+3 only test, atheros
	#
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch161_atheros	        root@"galileo$i":/home/root/meshnodeinit &	# TODO: MCS 0-3, atheros
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch5_ralink		        root@"galileo$i":/home/root/meshnodeinit &	# TODO: MCS 0-3, ralink
    #rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch13_ralink		        root@"galileo$i":/home/root/meshnodeinit &	# TODO: MCS 0-3, ralink
    ##rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch13_ralink_MCS3only   root@"galileo$i":/home/root/meshnodeinit &	# TODO: MCS 3, ralink, individ. tx pwr
    #rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch14_ralink_MCS3only    root@"galileo$i":/home/root/meshnodeinit &	# TODO: MCS 3, ralink, individ. tx pwr

    # TODO: Arne Ch161 CoAP measurements G31-35
    rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch161_noRTSCTS     root@"galileo$i":/home/root/meshnodeinit &	# TODO: without RTS/CTS
    #rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch161             root@"galileo$i":/home/root/meshnodeinit &	# TODO: with RTS/CTS

done

# ------------------------------------------------------------------------------

# interferers (MARCo)
##LIST="37 38"
LIST="" # TODO

for i in $LIST ; do

	echo "Copying to Galileo $i ..."

	rsync -a deploy/address_list.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/nf_list.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/phyNameDetectAndDevRename.sh root@"galileo$i":/home/root/ &

	# TODO: ch161 HT20 cfg. for CCA / ED test on interfered Atheros-based path -> adjust TX power manually after boot
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch161 root@"galileo$i":/home/root/meshnodeinit &
	#
	# TODO: ch3 HT20 cfg. for CCA / ED test on interfered Ralink-based path -> adjust TX power manually after boot
	#rsync -a deploy/meshnodeinit/meshnodeinit_minstrel_marcoRL_ch5 root@"galileo$i":/home/root/meshnodeinit &
	
	# TODO: ch165 HT40- for Atheros-based setup
	#rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch165_HT40-_interferer root@"galileo$i":/home/root/meshnodeinit &
	#
	# TODO: ch1 HT40+ for Ralink-based setup
	#rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch1_HT40+_interferer root@"galileo$i":/home/root/meshnodeinit &
    #
	# TODO: ch13 HT40- for Ralink-based setup
	rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch9_HT40+_interferer root@"galileo$i":/home/root/meshnodeinit &

done

# ------------------------------------------------------------------------------

# test nodes
##LIST="39 40"
LIST="" # TODO

for i in $LIST ; do

	echo "Copying to Galileo $i ..."

	rsync -a deploy/address_list.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/nf_list.sh root@"galileo$i":/home/root/ &
	rsync -a deploy/phyNameDetectAndDevRename.sh root@"galileo$i":/home/root/ &

	rsync -a deploy/meshnodeinit/meshnodeinit_fixedRates_ch36 root@"galileo$i":/home/root/meshnodeinit &

done

