#!/bin/bash

LIST="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29" # 31 32 33 34 35"

for i in $LIST ; do
	
	echo
	echo "Installing ptpd on Galileo $nr ..."

	xterm -T "Galileo $nr" -hold -e "ssh root@'galileo$i' -t 'bash -l -c \"/home/root/install_ptpd.sh\"'" &

	echo

done

